# devops team3

URL map:
****************************************
/admin/
    Django admin site

****************************************
/authenticate/
    Authenticate module
    Expected input:
    {
        "username":"XXXXXXXXXXXXX",
        "password":"YYYYYYYYYYYYY"
    }

    Expected outputs:
        Success:
        HTTP/1.0 200 OK
        {
            "token": "XXXXXXXXXXXXXXXX.YYYYYYYYYYYYYYYYYYY.ZZZZZZZZZZZZZZ"
        }

        Fail:
        HTTP/1.0 403 Forbidden
        {
            "message": "authentication failed"
        }

****************************************
/users/
    GET /users/ HTTP/1.1
        /users/
        /users/all 
            HTTP/1.0 200 OK
            [
                {
                    "nickname": "XXXXXXXX",
                    "id": id
                },
                {
                    "nickname": "YYYYYYYY",
                    "id": id
                },
            ]
        
        /users/me 
            HTTP/1.0 200 OK
            [
                {
                    "nickname": "LOGGED IN PLAYER",
                    "id": id
                }
            ]
        
        /users/NICK_NAME
            Success:
            HTTP/1.0 200 OK
            [
                {
                    "nickname": "PLAYER WITH NICK_NAME",
                    "id": id
                }
            ]
            Fail:
            HTTP/1.0 200 OK
            []
        
        /users/ID
            Success:
            HTTP/1.0 200 OK
            [
                {
                    "nickname": "PLAYER WITH ID",
                    "id": id
                }
            ]

            Fail:
            HTTP/1.0 200 OK
            []

    POST /users/ HTTP/1.1
        Not yeat implemented

****************************************
/tournaments/
    GET /tournaments/ HTTP/1.1
        /tournaments/
        /tournaments/all 
            Success:
            HTTP/1.0 200 OK
            [
                {
                    "games": [
                        {
                            "players": [], 
                            "id": 1, 
                            "name": "Eka testi peli"
                        }
                    ], 
                    "id": 1, 
                    "name": "Testi Turnee"
                }
            ]
        
        /tournaments/me 
            Success:
            HTTP/1.0 200 OK
            [
                {
                    "games": [
                        {
                            "players": [TO DO FIND OUT OUTPUT], 
                            "id": 1, 
                            "name": "Eka testi peli"
                        }
                    ], 
                    "id": 1, 
                    "name": "Testi Turnee"
                }
            ]

            FAIL:
            HTTP/1.0 200 OK
            []
        
        /tournaments/NICK_NAME 
            Success:
            HTTP/1.0 200 OK
            [
                {
                    "games": [
                        {
                            "players": [TO DO FIND OUT OUTPUT], 
                            "id": 1, 
                            "name": "Eka testi peli"
                        }
                    ], 
                    "id": 1, 
                    "name": "Testi Turnee"
                }
            ]

            FAIL:
            HTTP/1.0 200 OK
            []
        
        /tournaments/NICK_NAME 
            Success:
            HTTP/1.0 200 OK
            [
                {
                    "games": [
                        {
                            "players": [TO DO FIND OUT OUTPUT], 
                            "id": 1, 
                            "name": "Eka testi peli"
                        }
                    ], 
                    "id": 1, 
                    "name": "Testi Turnee"
                }
            ]

            FAIL:
            HTTP/1.0 200 OK
            []

    POST /tournaments/ HTTP/1.1
        Not yeat implemented

****************************************
/games/
    Not yeat implemented

****************************************
Authentication:
    All reguest made to paths not in TOKEN_AUTH_WHITELIST must require JsonWebToken signed by this server in http header "AUTHORIZATION".
    Expected input:
        "Authorization: XXXXXXXXXXXXX"

    Expected output:
        Fail:
            HTTP/1.0 403 Forbidden
            {
                "error": "no token"
            }

            HTTP/1.0 403 Forbidden
            {
                "error": "token not valid"
            }
    Settings.py:
        TOKEN_AUTH_WHITELIST =[]
