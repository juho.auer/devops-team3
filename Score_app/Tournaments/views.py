# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import View
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from db_models.models import Tournament
from db_models.models import Game
from db_models.models import Player
from authenticate.utils import get_user_from_token
import json
from django.core.exceptions import ObjectDoesNotExist

class Tournaments(View):
    def get(self, request, tournament_param=None):

        token = request.META['HTTP_AUTHORIZATION']

        if tournament_param is None or tournament_param=='all':
            tournaments = Tournament.objects.all()

            return HttpResponse(
                json.dumps([
                    tournament.tournament_to_json() for tournament in tournaments
                ]),
                content_type='application/json',
                status=200
            )

        elif tournament_param=='me':
            # To do get user from token
            # midleware not implemented yeat
            user = get_user_from_token(token)

            if user is not None:
                tournaments = Tournament.objects.filter(player=user.id)

                return HttpResponse(
                    json.dumps([
                        tournament.tournament_to_json() for tournament in tournaments
                    ]),
                    content_type='application/json',
                    status=200
                )
        else:
            # Determine integer or name
            is_integer = False
            is_name = False

            try:
                int(tournament_param)
                is_integer = True
            except ValueError:
                is_name = True

            # Try to get user from db
            try:
                if is_integer == True:
                    tournament = Tournament.objects.get(pk=tournament_param)
                elif is_name == True:
                    tournament = Tournament.objects.get(name=tournament_param)

                return HttpResponse(
                    json.dumps([
                        tournament.tournament_to_json()
                    ]),
                    content_type='application/json',
                    status=200
                )

            except ObjectDoesNotExist:
                return HttpResponse(
                    json.dumps([]),
                    content_type='application/json',
                    status=200
                )
