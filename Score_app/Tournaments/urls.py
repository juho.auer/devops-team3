from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.Tournaments.as_view()),
    url(r'^(?P<tournament_param>\w+)$', views.Tournaments.as_view()),
]
