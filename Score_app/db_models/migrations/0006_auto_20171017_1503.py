# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-17 15:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('db_models', '0005_auto_20171014_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='tournament',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='db_models.Tournament'),
        ),
        migrations.AlterField(
            model_name='player',
            name='games',
            field=models.ManyToManyField(null=True, to='db_models.Game'),
        ),
        migrations.AlterField(
            model_name='player',
            name='tournaments',
            field=models.ManyToManyField(null=True, to='db_models.Tournament'),
        ),
    ]
