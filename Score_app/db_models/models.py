# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
from django.db import models

@python_2_unicode_compatible
class Tournament(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.name

    def tournament_to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'games': [
                {
                    'id': game.id,
                    'name': game.name,
                    'players':[
                        {
                            'id': player.user_id,
                            'nickname': player.nickname
                        } for player in game.player_set.all()
                    ]
                } for game in self.game_set.all()
            ]
        }

# Create your models here.
@python_2_unicode_compatible
class Game(models.Model):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE, null=True, blank=True)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.name

class Player(models.Model):
    nickname = models.CharField(max_length=200, unique=True)
    join_date = models.DateTimeField('date joined')
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    games = models.ManyToManyField(Game, null=True, blank=True)
    tournaments = models.ManyToManyField(Tournament, null=True, blank=True)

    def __str__(self):
        return self.nickname

    def player_all_to_json(self):
        return {
            'id': self.user_id,
            'nickname': self.nickname,
            'tournaments': [
                {
                    'id': tournament.id,
                    'name': tournament.name,
                    'games': [
                        {
                            'id': game.id,
                            'name': game.name,
                            'players':[
                                {
                                    'id': player.user_id,
                                    'nickname': player.nickname
                                } for player in game.player_set.all()
                            ]
                        } for game in tournament.game_set.all()
                    ]
                } for tournament in self.tournaments.all()
            ],
            'games': [
                {
                    'id': game.id,
                    'name': game.name,
                    'players':[
                        {
                            'id': player.user_id,
                            'nickname': player.nickname
                        } for player in game.player_set.all()
                    ]
                } for game in self.games.all()
            ]
        }

    def player_to_json(self):
        return {
            'id': self.user_id,
            'nickname': self.nickname
        }
