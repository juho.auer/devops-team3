from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^', views.Authenticate.as_view()),
]
