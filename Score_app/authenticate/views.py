# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import authenticate
from django.conf import settings
import json
import jwt
from django.core.exceptions import ObjectDoesNotExist

class Authenticate(View):
    def post(self, request):
        username = None
        password = None
        try:
            body = json.loads(request.body)
            username = body['username']
            password = body['password']
        except:
            return HttpResponse(
                json.dumps({'message': 'No username or password'}),
                content_type='application/json',
                status=400
            )

        user = None

        # Authenticate user against django backend. Returns None if user not valid!
        try:
            user = authenticate(username=username, password=password)
        except:
            user = None

        if user is None:
            return HttpResponse(
                json.dumps({'message': 'authentication failed'}),
                content_type='application/json',
                status=403
            )
        else:
            # Get key from settings
            key = settings.TOKEN_SECRET

            # Encode token and return it in json response
            token = jwt.encode({
                'user_id': user.id,
                'username':user.username
            }, key, algorithm='HS256')

            return HttpResponse(
                json.dumps({'token': token}),
                content_type='application/json',
                status=200
            )

        return HttpResponse(
            json.dumps({'message': 'Unexpexted error'}),
            content_type='application/json',
            status=500
        )

    def get(self, request):
        return HttpResponse(
            json.dumps({'message': 'use post'}),
            content_type='application/json',
            status=200
        )
