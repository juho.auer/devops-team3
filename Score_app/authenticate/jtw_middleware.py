# -*- coding: utf-8 -*-
import jwt
import json
from django.conf import settings
from re import compile
from django.http import HttpResponse

if hasattr(settings, "TOKEN_AUTH_WHITELIST"):
    URLS = [compile(expr) for expr in settings.TOKEN_AUTH_WHITELIST]

class JwtMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # exclude white listed urls from token authentication
        path = request.path_info.lstrip("/")

        if any(u.match(path) for u in URLS):
            # One of the whitelisted urls matched. Request can continue.
            response = self.get_response(request)

        # All rest urls go trough token validation
        else:
            # Get token from header
            token = None
            try:
                token = request.META['HTTP_AUTHORIZATION']
            except KeyError:
                return HttpResponse(
                    json.dumps({'error': 'no token'}),
                    content_type='application/json',
                    status=403
                )

            # Get key from settings
            key = settings.TOKEN_SECRET

            # Try decode token. If ok continue with request.
            try:
                jwt.decode(token, key, algorithms=['HS256'])
            except:
                return HttpResponse(
                    json.dumps({'error': 'token not valid'}),
                    content_type='application/json',
                    status=403
                )

            # Token is valid Request can continue
            response = self.get_response(request)

        return response
