# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import Client
from django.test import TestCase

# Create your tests here.

class AuthenticateTests(TestCase):
    def setUp(self):
        pass
    
    def test_failed_authenticate(self):
        c = Client()
        response = c.post('/authenticate/', {'username': 'john', 'password': 'smith'})
        self.assertEqual(response.status_code, 400)
        
        