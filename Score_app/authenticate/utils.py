import jwt
from django.contrib.auth.models import User
from django.conf import settings

def get_user_from_token(token):

    key = settings.TOKEN_SECRET

    token_content = None

    try:
        token_content = jwt.decode(token, key, algorithms=['HS256'])
        print token_content
    except:
        return None

    try:
        user = User.objects.get(id=int(token_content['user_id']))
        print user
    except:
        return None

    return user
