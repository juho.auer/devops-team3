# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import View
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from db_models.models import Tournament
from db_models.models import Game
from db_models.models import Player
from authenticate.utils import get_user_from_token
import json
from django.core.exceptions import ObjectDoesNotExist

class Users(View):
    def get(self, request, user_param=None):

        token = request.META['HTTP_AUTHORIZATION']

        if user_param is None or user_param=='all':
            players = Player.objects.all()

            return HttpResponse(
                json.dumps([
                    player.player_to_json() for player in players
                ]),
                content_type='application/json',
                status=200
            )
        elif user_param=='me':
            # To do get user from token
            # midleware not implemented yeat
            user = get_user_from_token(token)

            if user is not None:
                player = Player.objects.get(pk=user.id)

                return HttpResponse(
                    json.dumps([
                        player.player_all_to_json()
                    ]),
                    content_type='application/json',
                    status=200
                )
            else:
                return HttpResponse(
                    json.dumps({'error': 'Token mismatch'}),
                    content_type='application/json',
                    status=400
                )

        elif user_param=='others':
            # To do get user from token
            # midleware not implemented yeat
            user = get_user_from_token(token)

            if user is not None:
                players = Player.objects.exclude(pk=user.id)

                return HttpResponse(
                    json.dumps([
                        player.player_to_json() for player in players
                    ]),
                    content_type='application/json',
                    status=200
                )
            else:
                return HttpResponse(
                    json.dumps({'error': 'Token mismatch'}),
                    content_type='application/json',
                    status=400
                )
        else:
            # Determine integer or name
            is_integer = False
            is_name = False

            try:
                int(user_param)
                is_integer = True
            except ValueError:
                is_name = True

            # Try to get user from db
            try:
                if is_integer == True:
                    player = Player.objects.get(pk=user_param)
                elif is_name == True:
                    player = Player.objects.get(nickname=user_param)

                return HttpResponse(
                    json.dumps([
                        player.player_all_to_json()
                    ]),
                    content_type='application/json',
                    status=200
                )

            except ObjectDoesNotExist:
                return HttpResponse(
                    json.dumps([]),
                    content_type='application/json',
                    status=200
                )
