from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.Users.as_view()),
    url(r'^(?P<user_param>\w+)$', views.Users.as_view()),
]
