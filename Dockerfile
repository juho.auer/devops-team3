FROM python:2.7

# Copy source code to /app directory in the container
COPY . /opt/Score_app

# Change working directory
WORKDIR /opt/Score_app

# Install dependencies
RUN pip install -r requirements.txt

# Expose API port to the outside
EXPOSE 8000

# Launch application
CMD ["python", "Score_app/manage.py", "runserver", "0.0.0.0:8000"]